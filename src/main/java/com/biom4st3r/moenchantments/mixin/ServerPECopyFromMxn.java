package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.biow0rks.Mxn;
import com.mojang.authlib.GameProfile;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.world.World;

@Mixin(ServerPlayerEntity.class)
public abstract class ServerPECopyFromMxn extends PlayerEntity {

    public ServerPECopyFromMxn(World world_1, GameProfile gameProfile_1) {
        super(world_1, gameProfile_1);
    }

    @Inject(at = @At(value = Mxn.At.BeforeFieldAccess, target = "enchantmentTableSeed"), method = "copyFrom")
    public void myInjection(ServerPlayerEntity serverPlayerEntity_1, boolean boolean_1, CallbackInfo ci)
    {
        this.inventory.clone(serverPlayerEntity_1.inventory);
    }

}