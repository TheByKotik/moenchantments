
package com.biom4st3r.moenchantments.mixin;

import java.util.Iterator;
import java.util.List;

import com.biom4st3r.moenchantments.MoEnchants;
import com.biom4st3r.moenchantments.Enchantments.MoEnchant;
import com.google.common.collect.Lists;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.InfoEnchantment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.registry.Registry;

@Mixin(EnchantmentHelper.class)
public abstract class EnchantmentHelperMixin {

   // @Inject(at = 
   //    @At(value = "INVOKE",
   //       opcode = Opcodes.INVOKEVIRTUAL,
   //       target = "net/minecraft/enchantment/Enchantment.isTreasure()Z"),
   // method = "getHighestApplicableEnchantmentsAtPower",locals = LocalCapture.CAPTURE_FAILHARD)
   // private static void getHighestApplicableEnchantmentsAtPower(int power, ItemStack iS, 
   //    boolean isTreasure, CallbackInfoReturnable<List<InfoEnchantment>> ci,List<InfoEnchantment> list_1, 
   //    Item item_1, boolean boolean_2, Iterator<Enchantment> var6, Enchantment enchantment)
   // {
   //    if(enchantment instanceof MoEnchant)
   //    {
   //       MoEnchant ench = (MoEnchant)enchantment;
   //       if(!ench.isAcceptableItem(iS))
   //       {

   //       }

   //    }
   // }

   
      @Overwrite
      public static List<InfoEnchantment> getHighestApplicableEnchantmentsAtPower(int EnchantmentPower, ItemStack targetItemStack, boolean isTreasureGeneration) 
      {
         List<InfoEnchantment> validEnchants = Lists.newArrayList();
         Item targetItem = targetItemStack.getItem();
         boolean isBook = targetItemStack.getItem() == Items.BOOK;
         Iterator<Enchantment> RegisteredEnchantments = Registry.ENCHANTMENT.iterator();
         
         while(RegisteredEnchantments.hasNext())
         {
            Enchantment currEnchantment = RegisteredEnchantments.next();
            if(!isBook)
            {
               if(currEnchantment instanceof MoEnchant)
               {
                  if(!((MoEnchant)currEnchantment).isAcceptableItem(targetItemStack))
                  {
                     continue;
                  }
               }
               else if(!currEnchantment.type.isAcceptableItem(targetItem))
               {
                  continue;
               }
            }
            if(currEnchantment.isTreasure() && !isTreasureGeneration)
            {
               continue;
            }
            for(int currEnchLvl = currEnchantment.getMaximumLevel(); currEnchLvl >= currEnchantment.getMinimumLevel(); --currEnchLvl) 
            {
               if (EnchantmentPower >= currEnchantment.getMinimumPower(currEnchLvl)) 
               {
                  if(currEnchantment == MoEnchants.ENDERPROTECTION)
                  {
                     validEnchants.add(new InfoEnchantment(currEnchantment, (currEnchLvl == 3 ? 1 : (currEnchLvl == 1 ? 3 : currEnchLvl))));
                  }
                  validEnchants.add(new InfoEnchantment(currEnchantment, currEnchLvl));
                  break;
               }
            }  
         }
         return validEnchants;
      }
}
