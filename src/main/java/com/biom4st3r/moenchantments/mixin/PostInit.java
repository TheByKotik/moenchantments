package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.biow0rks.Biow0rks;
import com.biom4st3r.biow0rks.Mxn;
import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.server.MinecraftServer;

@Mixin(MinecraftServer.class)
public abstract class PostInit
{

    @Inject(at = @At(Mxn.At.BeforeReturn),method="main")
    private static void postInit(CallbackInfo ci)
    {
        Biow0rks.log("...and my turn again");
        MoEnchantmentsMod.whitelistToBlock();
        
    }
}