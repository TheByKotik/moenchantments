package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;
import net.minecraft.item.ShovelItem;

public class VeinMinerEnchant extends MoEnchant {

    public VeinMinerEnchant() {
        super(Weight.RARE, EnchantmentTarget.DIGGER, new EquipmentSlot[] { EquipmentSlot.MAINHAND });

    }

    @Override
    public String getDisplayName() {
        return "Veining";
    }

    @Override
    public String regName() {
        return "veinminer";
    }

    @Override
    public int getMaximumLevel() {
        return MoEnchantmentsMod.config.VeinMinerMaxBreakByLvl.length;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        //return iS.getItem() instanceof PickaxeItem;
        Item i = iS.getItem();
        return (i instanceof MiningToolItem) && !(i instanceof ShovelItem || i instanceof AxeItem);
    }

    @Override
    public int getMinimumPower(int level) {
        return level * 5;
    }

    @Override
    public boolean enabled() {
        return MoEnchantmentsMod.config.EnableVeinMiner;
    }

}