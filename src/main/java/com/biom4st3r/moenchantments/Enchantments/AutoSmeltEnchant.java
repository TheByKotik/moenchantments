package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.SilkTouchEnchantment;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MiningToolItem;

public class AutoSmeltEnchant extends MoEnchant {

    public AutoSmeltEnchant() {
        super(Weight.VERY_RARE, EnchantmentTarget.DIGGER, new EquipmentSlot[] { EquipmentSlot.MAINHAND });
    }

    @Override
    public String regName() {
        return "autosmelt";
    }

    @Override
    public boolean differs(Enchantment enchantment_1) {

        return enchantment_1 instanceof SilkTouchEnchantment ? false : super.differs(enchantment_1);
    }

    @Override
    public String getDisplayName() {
        /*
         * Channeled Nether Nether Essence Hellfire Smelt Superheat Overheat Alphafire
         * AUTOSMELT
         */
        return "Alpha Fire";
    }

    @Override
    public int getMaximumLevel() {
        return 1;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }

    @Override
    public boolean enabled()
    {
        return MoEnchantmentsMod.config.EnableAutoSmelt;
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        Item i = iS.getItem();
        //return i instanceof AxeItem || i instanceof PickaxeItem || i instanceof ShovelItem;
        return i instanceof MiningToolItem;
    }

    @Override
    public int getMinimumPower(int level) {
        //return super.getMinimumPower(level);
        return level * 8;
    }



}