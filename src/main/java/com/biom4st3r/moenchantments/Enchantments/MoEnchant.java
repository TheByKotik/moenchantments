package com.biom4st3r.moenchantments.Enchantments;

import java.util.TreeMap;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.ListTag;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public abstract class MoEnchant extends Enchantment {

    public static final EquipmentSlot[] ALL = new EquipmentSlot[]{EquipmentSlot.HEAD, EquipmentSlot.CHEST, EquipmentSlot.LEGS, EquipmentSlot.FEET,EquipmentSlot.MAINHAND,EquipmentSlot.OFFHAND};

    private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();

    public MoEnchant(Weight w, EnchantmentTarget et, EquipmentSlot[] es) 
    {
        super(w, et, es);
    }

    public abstract String getDisplayName();

    public abstract String regName();

    @Override
    public Text getName(int level) 
    {
        Text component_1 = new TranslatableText(this.getTranslationKey(), new Object[0]);
        if (this.isCursed()) {
            component_1.formatted(Formatting.RED);
        } else {
            component_1.formatted(Formatting.GRAY);
        }
        if (level != 1 || this.getMaximumLevel() != 1) {
            component_1.append(" ").append(new TranslatableText(toRoman(level)));
        }
        return component_1;
    }

    public abstract boolean enabled();

    @Override
    public abstract int getMaximumLevel();

    @Override
    public abstract int getMinimumLevel();

    public abstract boolean isAcceptableItem(ItemStack iS);

    public static String toRoman(int num) {
        int l = map.floorKey(num);
        if (num == l) {
            return map.get(num);
        }
        return map.get(l) + toRoman(num - l);
    }

    // #region Statics
    static {
        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

    }
    // #endregion

    public static boolean hasEnchant(Enchantment e, ItemStack i) {
        return EnchantmentHelper.getLevel(e, i) > 0;
    }

    public static void removeEnchantment(ItemStack target, MoEnchant enchant) {
        
        if (target.getTag().containsKey("Enchantments", 9)) {
            ListTag enchants = target.getTag().getList("Enchantments",10);
            for(int i = 0; i < enchants.size(); i++)
            {
                String s = enchants.getCompoundTag(i).getString("id");
                if(s == String.valueOf(enchant.regName().toLowerCase()))
                {
                    enchants.remove(i);
                    break;
                }
            }
        }
     }
    
}