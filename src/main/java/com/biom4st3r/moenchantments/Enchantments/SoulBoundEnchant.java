package com.biom4st3r.moenchantments.Enchantments;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.item.ItemStack;

public class SoulBoundEnchant extends MoEnchant {

    public SoulBoundEnchant() {
        super(Weight.RARE, EnchantmentTarget.ALL, MoEnchant.ALL);
        // TODO Auto-generated constructor stub
    }

    @Override
    public String getDisplayName() {
        // TODO Auto-generated method stub
        return null;
        //PlayerInventory
    }

    @Override
    public String regName() {
        // TODO Auto-generated method stub
        return "soulbound";
    }

    @Override
    public boolean enabled() {
        // TODO Auto-generated method stub
        return true;
        //ServerPlayerEntity
        //PlayerEntity
        //ClientPlayerEntity
        //AbstractClientPlayerEntity
    }

    @Override
    public int getMaximumLevel() {
        return MoEnchantmentsMod.config.MaxSoulBoundLevel;
    }

    @Override
    public int getMinimumLevel() {
        return 1;
    }
    
    @Override
    public int getMinimumPower(int int_1) {
        return (int_1 == 1 ? 20*int_1 : Integer.MAX_VALUE);
    }

    @Override
    public boolean isAcceptableItem(ItemStack iS) {
        return true;
    }
    
}