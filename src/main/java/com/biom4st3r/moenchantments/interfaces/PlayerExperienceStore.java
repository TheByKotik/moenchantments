package com.biom4st3r.moenchantments.interfaces;

public interface PlayerExperienceStore
{
    public int getAndRemoveStoredExp();
    public void addExp(float i);
    


}