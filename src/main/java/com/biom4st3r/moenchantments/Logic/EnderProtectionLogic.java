package com.biom4st3r.moenchantments.Logic;

import com.biom4st3r.moenchantments.MoEnchantmentsMod;

import net.minecraft.command.arguments.EntityAnchorArgumentType.EntityAnchor;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.sound.SoundEvents;

public final class EnderProtectionLogic 
{
    public static boolean teleport(LivingEntity attacker, LivingEntity defender, boolean alwaysteleport)
    {
        if(alwaysteleport || defender.getRand().nextInt(100) + 1 <= MoEnchantmentsMod.config.chanceForEnderCurseToTeleport)
        {
            double x, y, z;
            x = attacker.x + (attacker.getRand().nextDouble() - 0.5D) * 8;
            y = attacker.y;// + (attacker.getRand().nextDouble() - 0.5D) * 3;
            z = attacker.z + (attacker.getRand().nextDouble() - 0.5D) * 8;
            //PlayerEntity  
            defender.playSound(SoundEvents.ENTITY_ENDERMAN_TELEPORT, 1f, 1f);
            defender.world.playSound((PlayerEntity)null, defender.prevX, defender.prevY, defender.prevZ, SoundEvents.ENTITY_ENDERMAN_TELEPORT, defender.getSoundCategory(), 1.0F, 1.0F);

            return defender.teleport(x, y, z, true);
        }
        return false;
        //EndermanEntity
    }

    public static void lookAtAttacker(LivingEntity attacker, LivingEntity defender)
    {
        defender.lookAt(EntityAnchor.FEET, attacker.getPos());
    }

    public static boolean preventDamage(LivingEntity defender)
    {
        int preventChance = MoEnchantmentsMod.config.chanceForEnderCurseToPreventDamage;
        if(defender.getRand().nextInt(100)+1 <=preventChance)
        {
            return true;
        }
        return false;
    }

    public static boolean doLogic(DamageSource damageSource,int protectionLvl,LivingEntity defender)
    {
        boolean teleported = false;
        if(damageSource.isProjectile())
        {
            switch(protectionLvl)
            {
                case 1:
                    //break;¯\_(ツ)_/¯
                case 2:
                    if(damageSource.getAttacker() != null)
                    {
                        LivingEntity attacker = (LivingEntity)damageSource.getAttacker();
                        teleported = EnderProtectionLogic.teleport(attacker, defender,true);
                        EnderProtectionLogic.lookAtAttacker(attacker, defender);
                    }
                    else
                    {
                        teleported = teleport(defender, defender,true);
                    }
                    break;
                default:
                    EnderProtectionLogic.teleport(defender, defender, true);
                    break;
            }
            if(protectionLvl < 3 && teleported)
            {
                return true;
            }
        }
        else if(damageSource.getAttacker() != null && damageSource.getAttacker() instanceof LivingEntity)
        {
            LivingEntity attacker = (LivingEntity)damageSource.getAttacker();
            switch(protectionLvl)
            {
                case 1:
                    teleported = EnderProtectionLogic.teleport(attacker, defender,false);
                    EnderProtectionLogic.lookAtAttacker(attacker, defender);
                    return EnderProtectionLogic.preventDamage(defender);
                case 2:
                    teleported = EnderProtectionLogic.teleport(attacker, defender,false);
                    EnderProtectionLogic.lookAtAttacker(attacker, defender);
                    break;
                default:
                teleported = EnderProtectionLogic.teleport(attacker, defender,false);
                    break;
                
            }
        }
        return false;
    }





}