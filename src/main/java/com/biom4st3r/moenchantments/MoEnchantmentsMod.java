package com.biom4st3r.moenchantments;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.biom4st3r.biow0rks.Biow0rks;
import com.biom4st3r.moenchantments.Items.DummyHeroProjectileItem;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class MoEnchantmentsMod implements ModInitializer {
	public static final String MODID = "biom4st3rmoenchantments";
	public static MoEnchantsConfig config;
	// public static EntityType<HerosProjectileEntity> herosProjectile = (EntityType<HerosProjectileEntity>) (Object) Registry
	// 		.register(Registry.ENTITY_TYPE, new Identifier("biom4st3r", "heroswipe"),
	// 				FabricEntityTypeBuilder.create(EntityCategory.MISC, (type, world) -> {
	// 					return new HerosProjectileEntity(world);
	// 				}).size(1.5f, 0.5f).build());;

	public static Identifier MISSEDATTACK = new Identifier(MoEnchantmentsMod.MODID, "missedattack");
	public static DummyHeroProjectileItem dhpi = new DummyHeroProjectileItem();
	public static final UUID uuidZero = new UUID(0L, 0L);
	public static List<Block> block_whitelist = new ArrayList<Block>();
	// public static List<Class<?>> class_whitelist = new ArrayList<Class<?>>();
	public static List<Item> autoSmelt_blacklist = new ArrayList<Item>();

	@Override
	public void onInitialize() {
		// #region config
		Biow0rks.DEBUG_MODE = false;
		File file = new File(FabricLoader.getInstance().getConfigDirectory().getPath(), "moenchantconfig.json");
		try {
			// util.logger("loading config!", true);
			Biow0rks.log("loading config!");
			FileReader fr = new FileReader(file);
			config = new Gson().fromJson(fr, MoEnchantsConfig.class);
			// FileWriter fw = new FileWriter(file);
			// fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
			// fw.close();
		} catch (IOException e) {
			Biow0rks.log("failed loading! Creating initial config!");
			// util.logger(, true);
			config = new MoEnchantsConfig();
			try {
				FileWriter fw = new FileWriter(file);
				fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
				fw.close();
			} catch (IOException e1) {
				Biow0rks.log("failed config!");
				// util.logger("failed config!", true);
				e1.printStackTrace();
			}
		}
		// #endregion

		Registry.register(Registry.ITEM, new Identifier("biom4st3r", "heroswipe"), dhpi);
		MoEnchants.init();

		// ServerSidePacketRegistry.INSTANCE.register(MISSEDATTACK, (packetContext, packetByteBuf) -> {

		// 	packetContext.getTaskQueue().execute(() -> {
		// 		PlayerEntity player = packetContext.getPlayer();
		// 		ItemStack weaponwithhero = player.getMainHandStack();
		// 		if (EnchantmentHelper.getLevel(MoEnchants.HEROSSWORD, weaponwithhero) > 0) {
		// 			Biow0rks.debug("MoEnchantmentsMod#onIntialize", "MISSEDATTACK packet summoned heroEntity");
		// 			player.world
		// 					.spawnEntity(new HerosProjectileEntity(player.world, player, player.x, player.y, player.z));
		// 			// player.world.spawnEntity(new Fireball(FIREBALL, player.world));
		// 		}
		// 	});

		// });
		blacklistAutosmelt();
	}

	public static boolean isWhiteListPopulated() {
		return isWhiteListPopulated;
	}

	private static void setWhiteListPopulated() {
		MoEnchantmentsMod.isWhiteListPopulated = true;
	}

	private static boolean isWhiteListPopulated = false;

	public static void whitelistToBlock() {
		setWhiteListPopulated();
		for (String s : config.veinMinerBlockWhiteList) {
			Block t = Registry.BLOCK.get(new Identifier(s));
			if (t != Blocks.AIR) {
				Biow0rks.log("added %s", t.getDropTableId().toString());
				block_whitelist.add(t);
			}
			else
			{
				Biow0rks.log("%s was not found", s );
			}
		}
	}

	public static void blacklistAutosmelt()
	{
		for(String s : config.AutoSmeltBlackList)
		{
			Item i = Registry.ITEM.get(new Identifier(s));
			if(i != Items.AIR)
			{
				autoSmelt_blacklist.add(i);
				Biow0rks.log("added %s to autosmelt blacklist",s);
			}
			else
			{
				Biow0rks.log("%s was not found", s);
			}
		}
	}

	// public static void whitelistToClass() {
	// 	for (String s : config.veinMinerClassWhiteList) {
	// 		try 
	// 		{
	// 			class_whitelist.add(Class.forName(s));
	// 			util.logger("added " + s, true);
	// 		} catch (ClassNotFoundException e) 
	// 		{
	// 			util.logger(s + " was not found", true);
	// 		}
	// 	}
	// }

	// public static void debug(String location, Object o)
	// {
	// 	if(isDebugBuild)
	// 	{
	// 		System.out.println(location + ": " + o.toString());
	// 	}
		
	// }




}